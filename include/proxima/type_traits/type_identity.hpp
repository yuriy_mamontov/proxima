#pragma once

namespace proxima
{
    /*!
        \~english
            \brief
                See C++20.

        \~russian
            \brief
                См. C++20.
     */
    template <typename T>
    struct type_identity
    {
        using type = T;
    };

    /*!
        \~english
            \brief
                See C++20.

        \~russian
            \brief
                См. C++20.
     */
    template <typename T>
    using type_identity_t = typename type_identity<T>::type;
} // namespace proxima
