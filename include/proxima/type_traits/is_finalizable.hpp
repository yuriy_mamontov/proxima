#pragma once

#include <proxima/type_traits/state_of.hpp>

#include <type_traits>

namespace proxima
{
    /*!
        \~english
            \brief
                Checks whether a transducer or a reduce kernel is finalizable

            \details
                A transducer or a reduce kernel is considered finalizable if the following function
                is defined:

                    is_final: K × S -> bool,

                where `K` is the reduce kernel or the transducer type, `S = state_of_t<K, A>` is the
                state type.

            \tparam K
                The transducer or the reduce kernel type.
            \tparam A
                The type of the symbol from which the state `S` of `K` has been initialized.

            \returns
                `true` if `is_final` is defined and `false` otherwise.

        \~russian
            \brief
                Проверка на финализируемость

            \details
                Ядро свёртки (или преобразователь) финализируемо, если определена функция

                    is_final: K × S -> bool,

                где `K` — тип преобразователя или ядра свёртки, `S = state_of_t<K, A>` — тип
                состояния.

            \tparam K
                Тип преобразователя или ядра свёртки.
            \tparam A
                Тип символа, которым было проинициализировано состояния `S`.

            \returns
                `true` в случае, если функция `is_final` определена, и `false` в противном случае.

        \~  \see is_final
            \see is_finalizable_v
     */
    template <typename K, typename A, typename = void>
    struct is_finalizable: std::false_type {};

    template <typename K, typename A>
    struct is_finalizable<K, A,
        std::enable_if_t
        <
            std::is_convertible
            <
                decltype(is_final
                (
                    std::declval<const K &>(),
                    std::declval<const state_of_t<K, A> &>()
                )),
                bool
            >
            ::value
        >>:
        std::true_type {};

    /*!
        \~english
            \brief
                Simplified form of finalizability check

            \tparam K
                The transducer or the reduce kernel type.
            \tparam A
                The symbol type.

            \returns
                `is_finalizable<K, A>::value`

        \~russian
            \brief
                Упрощённая форма проверки на финализируемость

            \tparam K
                Тип преобразователя или ядра свёртки.
            \tparam A
                Тип символа.

            \returns
                `is_finalizable<K, A>::value`

        \~  \see is_final
            \see is_finalizable
     */
    template <typename K, typename A>
    constexpr auto is_finalizable_v = is_finalizable<K, A>::value;
} // namespace proxima
