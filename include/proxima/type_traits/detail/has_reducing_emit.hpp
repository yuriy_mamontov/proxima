#pragma once

#include <proxima/type_traits/state_of.hpp>

#include <type_traits>

namespace proxima::detail
{
    template <typename K, typename A, typename = void>
    struct has_reducing_emit: std::false_type {};

    template <typename K, typename A>
    struct has_reducing_emit<K, A,
        std::void_t
        <
            decltype(emit(std::declval<const K &>(), std::declval<state_of_t<K, A> &>()))
        >>:
        std::true_type {};

    template <typename K, typename A>
    constexpr auto has_reducing_emit_v = has_reducing_emit<K, A>::value;
} // namespace proxima::detail
