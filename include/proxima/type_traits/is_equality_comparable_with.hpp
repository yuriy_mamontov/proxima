#pragma once

#include <initializer_list>
#include <type_traits>

namespace proxima
{
    template <typename T, typename U, typename = std::void_t<>>
    struct is_equality_comparable_with: std::false_type {};

    template <typename T, typename U>
    struct
        is_equality_comparable_with
        <
            T,
            U,
            std::void_t
            <
                decltype(static_cast<bool>(std::declval<const T &>() == std::declval<const U &>())),
                decltype(static_cast<bool>(std::declval<const U &>() == std::declval<const T &>())),
                decltype(static_cast<bool>(std::declval<const T &>() != std::declval<const U &>())),
                decltype(static_cast<bool>(std::declval<const U &>() != std::declval<const T &>()))
            >
        >: std::true_type {};

    template <typename T, typename U>
    constexpr auto is_equality_comparable_with_v = is_equality_comparable_with<T, U>::value;
} // namespace proxima
