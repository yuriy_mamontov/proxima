#pragma once

#include <proxima/type_traits/state_of.hpp>

#include <type_traits>

namespace proxima
{
    /*!
        \~english
            \brief
                Checks for reduction operation existence

            \details
                Class `K` considered reducible over the symbol `A` if the following member function
                is defined:

                    K: A × S -> void,

                where `A` is the symbol type, `S = state_of<K, A>` is the state type.

            \tparam K
                Potential reduce kernel type.
            \tparam A
                The input symbol type.

            \returns
                `true` if `K(A, S)` call is defined and `false` otherwise.

        \~russian
            \brief
                Проверка на наличие операции свёртки

            \details
                Класс `K` обладает операцией свёртки над символом `A`, если определена
                функция-член

                    K: A × S -> void,

                где `A` — тип символа, `S = state_of<K, A>` — тип состояния.

            \tparam K
                Тип потенциального ядра свёртки.
            \tparam A
                Тип входного символа.

            \returns
                `true` в случае, если вызов `K(A, S)` определён, и `false` в противном случае.

        \~  \see is_reducible_v
            \see state_symbol_t
     */
    template <typename K, typename A, typename = void>
    struct is_reducible: std::false_type {};

    template <typename K, typename A>
    struct is_reducible<K, A,
        std::enable_if_t
        <
            std::is_invocable_r_v<void, K, A, state_of_t<K, A> &>
        >>:
        std::true_type {};

    /*!
        \~english
            \brief
                Simplified form of reducibility checking

            \tparam K
                Potential reduce kernel type.
            \tparam A
                The symbol type.

            \returns
                `is_reducible<K, A>::value`

        \~russian
            \brief
                Упрощённая форма проверки на наличие операции свёртки

            \tparam K
                Тип потенциального ядра свёртки.
            \tparam A
                Тип символа.

            \returns
                `is_reducible<K, A>::value`

        \~  \see is_reducible
     */
    template <typename K, typename A>
    constexpr auto is_reducible_v = is_reducible<K, A>::value;
} // namespace proxima
