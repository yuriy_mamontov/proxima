#pragma once

#include <proxima/type_traits/detail/black_hole.hpp>
#include <proxima/type_traits/is_stateless.hpp>

#include <type_traits>

namespace proxima
{
    /*!
        \~english
            \brief
                Checks for transduce operation existence

            \details
                Class `T` considered transducible over symbol `A` if the following member function
                is defined:

                    T: A × Tp × S -> void // For the stateful transducer.
                    T: A × Tp -> void, // For the stateless transducer.

                where `A` is the symbol type, `Tp` is the output tape type, `S = state_of_t<T, A>`
                is the state type.

                Type of the tape does not matter. It matters only that it consumes the output of
                the transducer. Therefore, the check uses an "omnivorous" tape, and the tape is
                not exposed in the arguments of the metafunction.

            \tparam T
                Potential transducer type.
            \tparam A
                Symbol type.

            \returns
                `true` if `T(A, Tp, S)` or `T(A, Tp)` call is defined and `false` otherwise.

        \~russian
            \brief
                Проверка на наличие операции преобразования

            \details
                Говорят, что класс `T` обладает операцией преобразования над символом `A`, если
                определена функция-член

                    T: A × Tp × S -> void // Для преобразователя с состоянием.
                    T: A × Tp -> void, // Для преобразователя без состояния.

                где `A` — тип символа, `Tp` — тип выходной ленты,`S = state_of_t<T, A>` — тип
                состояния.

                Тип ленты не имеет значения. Важно только то, что на неё записывается символ,
                получаемый с выхода преобразователя. Поэтому в проверке используется "всеядная"
                лента, а в аргументах метафункции она и вовсе отсутствует.

            \tparam T
                Тип потенциального преобразователя.
            \tparam A
                Тип символа.

            \returns
                `true` в случае, если вызов `T(A, Tp, S)` или `T(A, Tp)` определён,
                и `false` в противном случае.

        \~  \see is_transducible_v
            \see is_reducible
            \see state_of
            \see is_stateful
            \see is_stateless
     */
    template <typename T, typename A, typename = void>
    struct is_transducible: std::false_type {};

    template <typename T, typename A>
    struct is_transducible<T, A,
        std::enable_if_t
        <
            is_stateful_v<T, A> &&
            std::is_invocable_r_v<void, T, A, detail::black_hole_t, state_of_t<T, A> &>
        >>:
        std::true_type {};

    template <typename T, typename A>
    struct is_transducible<T, A,
        std::enable_if_t
        <
            is_stateless_v<T, A> &&
            std::is_invocable_r_v<void, T, A, detail::black_hole_t>
        >>:
        std::true_type {};

    /*!
        \~english
            \brief
                Simplified form of tranducibility checking

            \tparam T
                The potential transducer type.
            \tparam A
                The symbol type.

            \returns
                `is_transducible<T, A>::value`

        \~russian
            \brief
                Упрощённая форма проверки на наличие операции преобразования

            \tparam T
                Тип потенциального преобразователя.
            \tparam A
                Тип символа.

            \returns
                `is_transducible<T, A>::value`

        \~  \see is_transducible
     */
    template <typename T, typename A>
    constexpr auto is_transducible_v = is_transducible<T, A>::value;
} // namespace proxima
