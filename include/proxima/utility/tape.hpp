#pragma once

#include <proxima/type_traits/is_reducible.hpp>
#include <proxima/type_traits/state_symbol.hpp>

#include <type_traits>
#include <utility>

namespace proxima
{

    /*!
        \~english
            \brief
                A helper class to introduce the tape concept in the code

            \details
                When a transducer passes the result of its work further down the chain, it does not
                know a type of the reduce kernel to which it is passing data. It just writes data
                on the tape, and does not worry about who reads it.
                This class is designed to wrap the reduce kernel together with its state in an
                independent object, which is enough to pass the value to, in other words,
                "write the data on the tape".

            \tparam K
                The type of the wrapped reduce kernel.
            \tparam S
                The state type of the wrapped reduce kernel.

        \~russian
            \brief
                Вспомогательный класс для представления концепции ленты в коде

            \details
                Когда преобразователь отдаёт результат своей работы далее по цепочке, он не знает
                конкретного типа ядра свёртки, которому он передаёт данные. Он знает только то,
                что пишет данные на ленту, а кто читает с этой ленты — не важно.
                Данный класс предназначен именно для того, чтобы завернуть ядро свёртки вместе с
                его состоянием в самостоятельный объект, которому достаточно отдать значение, то
                есть "записать на ленту".

            \tparam K
                Тип заворачиваемого ядра свёртки.
            \tparam S
                Тип состояния заворачиваемого ядра свёртки.

        \~  \see is_reducible
     */
    template <typename K, typename S>
    struct tape_t
    {
        template <typename A>
        constexpr void operator () (A && symbol)
        {
            kernel(std::forward<A>(symbol), state);
        }

        const K & kernel;
        S & state;
    };

    template <typename K, typename S, typename =
        std::enable_if_t<is_reducible_v<K, state_symbol_t<S>>>>
    constexpr auto make_tape (const K & kernel, S & state)
    {
        return tape_t<K, S>{kernel, state};
    }
} // namespace proxima
