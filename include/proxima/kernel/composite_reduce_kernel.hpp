#pragma once

#include <proxima/state/composite_state.hpp>
#include <proxima/state/nullstate.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/is_emitting.hpp>
#include <proxima/type_traits/is_finalizable.hpp>
#include <proxima/type_traits/is_stateful.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <proxima/utility/is_finalizable_and_final.hpp>
#include <proxima/utility/tape.hpp>

#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Composite reduce kernel

            \details
                Conceptually is a "matryoshka" that consists of a tranducer (the outer part of the
                "matryoshka") and a reduce kernel (the inner part of the "matryoshka").

                Behaves just like any other reduce kernel: has a state and a defined `initialize`
                function that creates this state, as well as the call operator that takes the
                approproate symbol and state.

                Composite reduce kernel takes over the properties of the transducer and the reduce
                kernel, from which it was created. For example, if the transducer or the reduce
                kernel is finalizable, then composite reduce kernel will also be finalizable.

            \tparam T
                The transducer forming the outer part of the "matryoshka".
            \tparam K
                The reduce kernel forming the inner part of the "matryoshka".

        \~russian
            \brief
                Составное ядро свёртки

            \details
                Представляет собой "матрёшку", состоящую из преобразователя (внешней части
                "матрёшки") и ядра свёртки (внутренней части "матрёшки").

                Ведёт себя так же, как и любое другое ядро свёртки: имеет состояние и определённую
                функцию `initialize`, создающую это состояние, а также оператор вызова, принимающий
                соответствующие символ и состояние.

                Составное ядро свёртки перенимает свойства преобразователя и ядра свёртки, из
                которых было создано. Например, если хотя бы один из них имеет раннее завершение,
                то полученное составное ядро свёртки тоже будет иметь раннее завершение.

            \tparam T
                Преобразователь представляющий внешнюю часть "матрёшки".
            \tparam K
                Ядро свёртки, представляющее внутреннюю часть "матрёшки".

        \~  \see compose
            \see composite_state_t
     */
    template <typename T, typename K>
    struct composite_reduce_kernel_t
    {
        template <typename A, typename S>
        constexpr void operator () (A && symbol, S & state) const
        {
            auto t = make_tape(tail, state.tail);
            if constexpr (is_stateful_v<T, std::decay_t<A>>)
            {
                head(std::forward<A>(symbol), t, state.head);
            }
            else
            {
                static_assert(std::is_same_v<decltype(state.head), nullstate_t>);
                head(std::forward<A>(symbol), t);
            }
        }

        T head;
        K tail;
    };

    /*!
        \~english
            \brief
                Initialize the composite reduce kernel state

            \param k
                The composite reduce kernel.
            \param type
                A tag containing the type of a symbol which is used to produce the initial state of
                the reduce kernel.

            \returns
                The initial state of the composite reduce kernel.

        \~russian
            \brief
                Инициализация состояния составного ядра свёртки

            \param k
                Составное ядро свёртки.
            \param type
                Метка, содержащая тип символа, которым будет проинициализировано начальное
                состояние составного ядра свёртки.

            \returns
                Начальное состояние составного ядра свёртки.

        \~  \see composite_reduce_kernel_t
            \see type_t
            \see compose
     */
    template <typename T, typename K, typename A>
    constexpr auto initialize (const composite_reduce_kernel_t<T, K> & k, type_t<A> /* type */)
    {
        using output_symbol = output_symbol_t<T, A>;

        if constexpr (is_stateful_v<T, A>)
        {
            return
                composite_state_t<T, K, A>
                {
                    initialize(k.head, type<A>),
                    initialize(k.tail, type<output_symbol>)
                };
        }
        else
        {
            return
                composite_state_t<T, K, A>
                {
                    nullstate,
                    initialize(k.tail, type<output_symbol>)
                };
        }
    }

    /*!
        \~english
            \brief
                Check for early stop of a composite reduce kernel

            \details
                This function is defined if and only if one (or both) of the transducer `T` and the
                reduce kernel `K`, forming the composite reduce kernel `k`, are finalizable on the
                corresponding states, forming the composite state `s`.

                I.e. `is_final` is defined for `composite_reduce_kernel_t<T, K>` if and only if
                `is_final` is defined for `T` or `is_final` is defined for `K`.

            \param k
                The composite reduce kernel.
            \param s
                The state of the composite reduce kernel `k`.

            \tparam T, K
                A transducer and a reduce kernel, from which the composite reduce kernel `k`
                consists.
            \tparam A
                The type of the initial symbol from which the state `s` of the composite reduce
                kernel `k` has been initialized.

            \returns
                `true` if reducing is over, that is, the final state has been reached, starting
                from which the reduction process is undefined, and `false` otherwise.

        \~russian
            \brief
                Проверка на раннее завершение составного ядра свёртки

            \details
                Данная функция определена тогда, и только тогда, когда преобразователь или ядро
                свёртки (или оба), образующие составное ядро свёртки `k`, имеют свойство раннего
                завершения на соответствующих им состояниях, образующих составное состояние `s`.

                Говоря формально, `is_final` определена для `composite_reduce_kernel_t<T, K>`
                тогда, и только тогда, когда `is_final` определена для `T` или `is_final`
                определена для `K`.

            \param k
                Составное ядро свёртки.
            \param s
                Состояние составного ядра свёртки `k`.

            \tparam T, K
                Преобразователь и ядро свёртки, образующие составное ядро свёртки `k`.
            \tparam A
                Тип начального символа, которым было проинициализировано состояние `s`
                составного ядра свёртки `k`.

            \returns
                `true`, если процесс свёртки окончен, то есть достигнуто финальное состояние,
                начиная с которого процесс свёртки неопределён, и `false` в противном случае.

        \~  \see composite_reduce_kernel_t
            \see composite_state_t
            \see is_finalizable
            \see compose
     */
    template <typename T, typename K, typename A>
    constexpr auto
        is_final (const composite_reduce_kernel_t<T, K> & k, const composite_state_t<T, K, A> & s)
        -> std::enable_if_t
            <
                is_finalizable_v<T, A> || is_finalizable_v<K, output_symbol_t<T, A>>,
                bool
            >
    {
        if constexpr (is_finalizable_v<T, A> && is_finalizable_v<K, output_symbol_t<T, A>>)
        {
            return is_final(k.head, s.head) || is_final(k.tail, s.tail);
        }
        else if constexpr (is_finalizable_v<T, A>)
        {
            return is_final(k.head, s.head);
        }
        else
        {
            return is_final(k.tail, s.tail);
        }
    }

    /*!
        \~english
            \brief
                Emit stored value of a composite reduce kernel state

            \details
                This function is defined if and only if one (or both) of the transducer `T` and the
                reduce kernel `K`, forming the composite reduce kernel `k`, are emitting on the
                corresponding states, forming the composite state `s`.

                I.e. `emit` is defined for `composite_reduce_kernel_t<T, K>` if and only if `emit`
                is defined for `T` or `emit` is defined for `K`.

            \param k
                The composite reduce kernel.
            \param s
                The state of the composite reduce kernel `k`.

            \tparam T, K
                A transducer and a reduce kernel, from which the composite reduce kernel `k`
                consists.
            \tparam A
                The type of the initial symbol from which the state `s` of the composite reduce
                kernel `k` has been initialized.

        \~russian
            \brief
                Высвободить запомненное значение состояния составного ядра свёртки

            \details
                Данная функция определена тогда, и только тогда, когда преобразователь или ядро
                свёртки (или оба), образующие составное ядро свёртки `k`, имеют свойство завершения
                по требованию на соответствующих им состояниях, образующих составное состояние `s`.

                Говоря формально, `emit` определена для `composite_reduce_kernel_t<T, K>` тогда, и
                только тогда, когда `emit` определена для `T` или `emit` определена для `K`.

            \param k
                Составное ядро свёртки.
            \param s
                Состояние составного ядра свёртки `k`.

            \tparam T, K
                Преобразователь и ядро свёртки, образующие составное ядро свёртки `k`.
            \tparam A
                Тип начального символа, которым было проинициализировано состояние `s`
                составного ядра свёртки `k`.

        \~  \see composite_reduce_kernel_t
            \see composite_state_t
            \see is_emitting
            \see compose
     */
    template <typename T, typename K, typename A>
    constexpr auto
        emit (const composite_reduce_kernel_t<T, K> & k, composite_state_t<T, K, A> & s)
        -> std::enable_if_t
            <
                is_emitting_v<T, A> || is_emitting_v<K, output_symbol_t<T, A>>
            >
    {
        if constexpr (is_emitting_v<T, A>)
        {
            if (not is_finalizable_and_final(k.tail, s.tail))
            {
                auto t = make_tape(k.tail, s.tail);
                emit(k.head, t, s.head);
            }
        }

        if constexpr (is_emitting_v<K, output_symbol_t<T, A>>)
        {
            emit(k.tail, s.tail);
        }
    }
} // namespace proxima
