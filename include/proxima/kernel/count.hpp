#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <cstddef>

namespace proxima
{
    /*!
        \~english
            \brief
                The counting reduce kernel

            \details
                Counts the amount of elements that are read from the input tape.

            \tparam Integral
                The type of the counter.

        \~russian
            \brief
                Считающее ядро свёртки

            \details
                Вычисляет количество символов, считанных со входной ленты.

            \tparam Integral
                Тип счётчика.
     */
    template <typename Integral>
    struct count_t
    {
        /*!
            \~english
                \brief
                    Reads a symbol from the input tape and increments the counter

                \details
                    Performs the `operator ++` on the current value of the state.

                \param symbol
                    The symbol from the input tape.
                \param state
                    The current state of the reduce kernel. Contains count of the elements so far.

            \~russian
                \brief
                    Считывает символ со входной ленты и увеличивает счётчик

                \details
                    Выполняет вызов оператора инкремента над текущим значением состояния.

                \param symbol
                    Символ со входной ленты.
                \param state
                    Текущее состояние ядра свёртки. Содержит текущее количество элементов.
         */
        template <typename A, typename S>
        constexpr void operator () (A && /* symbol */, S & state) const
        {
            ++value(state);
        }

        /*!
            \~english
                \brief
                    Creates a reduce kernel with custom counter

                \tparam Integral1
                    The type of the custom counter.

                \param n
                    The initial value of the custom counter.

                \returns
                    An instance of `count_t` with custom counter.

            \~russian
                \brief
                    Создать ядро свёртки с пользовательским счётчиком

                \tparam Integral1
                    Тип пользовательского счётчика.

                \param n
                    Начальное значение пользовательского счётчика.

                \returns
                    Экземпляр ядра свёртки `count_t` с заданным счётчиком.
         */
        template <typename Integral1>
        constexpr auto operator () (Integral1 n) const
        {
            return count_t<Integral1>{n};
        }

        Integral initial;
    };

    /*!
        \~english
            \brief
                A convenience instrument for using the counting reduce kernel

            \details
                Usage examples:

                \code{.cpp}
                auto count = proxima::reduce(sequence, proxima::count);
                \endcode

                \code{.cpp}
                auto count = proxima::reduce(sequence, proxima::count(unsigned{5}));
                \endcode

                \code{.cpp}
                auto count_of_unique_elements =
                    proxima::compose
                    (
                        proxima::unique,
                        proxima::count
                    );
                auto count = proxima::reduce(sequence, count_of_unique_elements);
                \endcode

            \returns
                An instance of `count_t` reduce kernel.

        \~russian
            \brief
                Инструмент для удобного создания считающей свёртки

            \details
                Примеры использования:

                \code{.cpp}
                auto count = proxima::reduce(sequence, proxima::count);
                \endcode

                \code{.cpp}
                auto count = proxima::reduce(sequence, proxima::count(unsigned{5}));
                \endcode

                \code{.cpp}
                auto count_of_unique_elements =
                    proxima::compose
                    (
                        proxima::unique,
                        proxima::count
                    );
                auto count = proxima::reduce(sequence, count_of_unique_elements);
                \endcode

            \returns
                Экземпляр ядра свёртки `count_t`.

        \~  \see count_t
            \see compose
            \see reduce
     */
    constexpr auto count = count_t<std::size_t>{0};

    template <typename Integral, typename A>
    constexpr auto initialize (const count_t<Integral> & k, type_t<A>)
    {
        return simple_state_t<Integral, A>{k.initial};
    }
}
