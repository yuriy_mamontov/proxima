#pragma once

#include <proxima/kernel/composite_reduce_kernel.hpp>
#include <proxima/reduce.hpp>
#include <proxima/type_traits/output_symbol.hpp>

#include <iterator>

namespace proxima
{
    /*!
        \~english
            \brief
                A joining transducer

            \details
                An input symbol of this transducer is a range. Thus the transducer takes every
                element of the range and writes it to the output tape.
                The transducer is stateless, is not finalizable and is not emitting.

        \~russian
            \brief
                Преобразователь, разматывающий набор диапазонов в сплошной диапазон

            \details
                Входной символ — диапазон. По определению преобразователь пробегает по всему
                диапазону и выписывает на ленту все значения из него. У этого преобразователя
                нет состояния, он не финализируем, не является испускающим. Кроме этого,
                по определённым причинам он просто является тегом, который сообщит составному
                ядру свёртки, что нужно применяться иначе.

        \~  \see join
     */
    struct join_t {};

    /*!
        \~english
            \brief
                An overload of the `output_symbol` metafunction for the `join_t` transducer

            \details
                Since the input symbol of the `join_t` transducer is a range, the type of the output
                symbol is the type of the element of that range.

            \tparam Symbol
                The type of the input symbol.

            \returns
                The type of elements of the `Symbol` range.

        \~russian
            \brief
                Перегрузка метафункции `output_symbol`

            \details
                Поскольку `join_t` принимает символом диапазон, то, согласно описанию, тип
                выходного символа — тип элемента в этом диапазоне.

            \tparam Symbol
                Тип входного символа. Является диапазоном.

            \returns
                Тип элементов, из которых состоит входной символ-диапазон `Symbol`.

        \~  \see join_t
            \see output_symbol
     */
    template <typename Symbol>
    struct output_symbol<join_t, Symbol>
    {
        using range_type = Symbol;
        using iterator = decltype(std::begin(std::declval<range_type>()));
        using type = typename std::iterator_traits<iterator>::value_type;
    };

    /*!
        \~english
            \brief
                Obtain an instance of `join_t` transducer

        \~russian
            \brief
                Получить экземпляр `join_t`

        \~  \see join_t
     */
    constexpr auto join = join_t{};

    /*!
        \~english
            \brief
                A specialization of the composite reduce kernel for the joining transducer

            \details
                The joining transducer potentially writes more symbols to the output tape than it
                reads from the input tape, and a single step of the `join_t` may produce more
                symbols than the next transducer in the chain accepts. Thus the joining transducer
                must be treated in a special way.

        \~russian
            \brief
                Специальная перегрузка разложимого ядра свёртки

            \details
                Рассмотрим следующий код:

                \code{.cpp}
                auto values = std::vector<std::array<int, 10>>{...};
                assert(not values.empty());
                auto k =
                    proxima::compose
                    (
                        proxima::join,
                        proxima::take(3), <-- assert(3 < 10);
                        proxima::to_vector
                    );
                auto result = proxima::reduce(values, k);
                \endcode

                Если бы `join_t` был устроен как остальные преобразователи, т.е. просто
                выписывал (раскручивал) на ленту те символы-диапазоны, которые он получил, то
                из-за того, что шаг `reduce` — целый диапазон, то мы бы имели в примере выше
                неопределённое поведение. Чтобы избежать такой ситуации в будущем планируется
                использовать сопрограммы, а пока что будет специальная перегрузка ядра-композита
                под данный преобразователь-тег. Все потроха диапазона-символа, который получил
                на вход `join` будут заброшены в `reduce` вместе с текущим состоянием и
                ядром-хвостом.

        \~  \see join_t
            \see reduce
     */
    template <typename K>
    struct composite_reduce_kernel_t<join_t, K>
    {
        template <typename A, typename S>
        constexpr void operator () (A && symbol, S & state) const
        {
            auto update = proxima::reduce(std::forward<A>(symbol), std::move(state.tail), tail);
            state.tail = std::move(update);
        }

        // Заглушка для `compose`.
        join_t head;
        K tail;
    };
}
