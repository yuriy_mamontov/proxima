#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <cassert>
#include <cstddef>
#include <type_traits>
#include <utility>
#include <vector>

namespace proxima
{
    /*!
        \~english
            \brief
                A chunk transducer

            \details
                Breaks an input sequence into chunks of a specified size.

        \~russian
            \brief
                Кусочный преобразователь

            \details
                Разбивает входящую последовательность на куски заданного размера.

        \~  \see chunk
     */
    struct chunk_t
    {
        template <typename Symbol>
        using output_symbol_t = std::vector<Symbol>;

        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape and pushes it to the end of the container.
                    If then there are exactly `N` elements in the container, moves the container
                    to the output tape.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The result will be written to this tape, if necessary.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты и дописывает его в конец контейнера.
                    Если после всего этого в контейнере ровно `N` элементов, то контейнер
                    переносится на выходную ленту.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую при необходимости будет записан результат.
                \param state
                    Текущее состояние преобразователя.
         */
        template <typename A, typename T, typename S>
        constexpr void operator () (A && symbol, T && tape, S & state) const
        {
            value(state).push_back(std::forward<A>(symbol));

            if (value(state).size() == chunk_size)
            {
                std::forward<T>(tape)(std::move(value(state)));
                value(state).clear();
            }
        }

        std::size_t chunk_size;
    };

    template <typename T>
    constexpr auto initialize (const chunk_t &, type_t<T>)
    {
        return simple_state_t<std::vector<T>, T>{};
    }

    template <typename UnaryFunction, typename State>
    constexpr void emit (const chunk_t &, UnaryFunction && tape, State & state)
    {
        if (not value(state).empty())
        {
            std::forward<UnaryFunction>(tape)(std::move(value(state)));
        }
    }

    /*!
        \~english
            \brief
                Creates a chunk transducer

            \param n
                A chunk size.

            \pre
                `n > 0`

            \returns
                An instance of `chunk_t` with the specified chunk size.

        \~russian
            \brief
                Создать кусочный преобразователь

            \param n
                Размер куска.

            \pre
                `n > 0`

            \returns
                Экземпляр преобразователя `chunk_t` с куском указанного размера.

        \~  \see chunk_t
     */
    template <typename Integral>
    constexpr auto chunk (Integral n)
    {
        static_assert(std::is_integral_v<Integral>);
        assert(n > Integral{0});
        return chunk_t{static_cast<std::size_t>(n)};
    }
}
