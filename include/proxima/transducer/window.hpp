#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <cassert>
#include <deque>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                A sliding window transducer

            \details
                For each element starting with `N`, generates and writes to the output tape the
                range of `N` last symbols from the input tape. `N` is the size of the sliding
                window.

        \~russian
            \brief
                Оконный преобразователь

            \details
                Для каждого элемента, начиная с `N`-го, порождает и записывает на выходную ленту
                диапазон из `N` последних символов со входной ленты. `N` — размер скользящего окна.

        \~  \see window
     */
    struct window_t
    {
        template <typename Symbol>
        using output_symbol_t = std::deque<Symbol>;

        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape and pushes it to the end of the container.
                    If the container then contains `N + 1` elements, then the first of them is
                    popped.
                    If then there are exactly `N` elements in the container, then the container
                    is written to the output tape.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The result will be written to this tape, if necessary.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты и дописывает его в конец контейнера.
                    Если в контейнере стало `N + 1` элементов, то самый первый из них выбрасывается.
                    Если после всего этого в контейнере ровно `N` элементов, то контейнер
                    записывается на выходную ленту.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую при необходимости будет записан результат.
                \param state
                    Текущее состояние преобразователя.
         */
        template <typename A, typename T, typename S>
        constexpr void operator () (A && symbol, T && tape, S & state) const
        {
            value(state).push_back(std::forward<A>(symbol));

            if (value(state).size() > window_size)
            {
                value(state).pop_front();
            }

            if (value(state).size() == window_size)
            {
                std::forward<T>(tape)(value(state));
            }
        }

        std::size_t window_size;
    };

    template <typename T>
    constexpr auto initialize (const window_t &, type_t<T>)
    {
        return simple_state_t<std::deque<T>, T>{};
    }

    /*!
        \~english
            \brief
                Creates a sliding window transducer

            \param n
                The sliding window size.

            \pre
                `n > 0`

            \returns
                An instance of `window_t`.

        \~russian
            \brief
                Создать оконный преобразователь

            \param n
                Размер скользящего окна.

            \pre
                `n > 0`

            \returns
                Экземпляр преобразователя `window_t` с окном указанного размера.

        \~  \see window_t
     */
    template <typename Integral>
    constexpr auto window (Integral n)
    {
        static_assert(std::is_integral_v<Integral>);
        assert(n > Integral{0});
        return window_t{static_cast<std::size_t>(n)};
    }
}
