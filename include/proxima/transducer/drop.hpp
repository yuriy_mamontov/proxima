#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <utility>
#include <cassert>

namespace proxima
{
    /*!
        \~english
            \brief
                A transducer that drops the fixed length prefix of a sequence

            \tparam Integer
                The type of the counter of a dropped prefix.

        \~russian
            \brief
                Преобразователь, отбрасывающий префикс последовательности заданного размера

            \tparam Integer
                Тип счётчика длины отброшенного префикса.

        \~  \see drop
            \see take_t
            \see take_if_t
            \see drop_if_t
            \see take_while_t
            \see drop_while_t
     */
    template <typename Integer>
    struct drop_t
    {
        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape and checks if the whole prefix is already
                    dropped. If it is so, then the symbol is written to the output tape. Otherwise
                    the symbol is ignored, i.e. transducer decreases the counter and does not write
                    anything to the output tape.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The result will be written to this tape, if necessary.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты и, если весь требуемый префикс уже отброшен,
                    то есть счётчик дошёл до нуля, записывает этот символ на выходную ленту.
                    В противном случае символ игнорируется, то есть на выходную ленту ничего не
                    пишется, а счётчик уменьшается.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую при необходимости будет записан результат.
                \param state
                    Текущее состояние преобразователя.
         */
        template <typename A, typename T, typename S>
        constexpr void operator () (A && symbol, T && tape, S & state) const
        {
            if (value(state) == 0)
            {
                std::forward<T>(tape)(std::forward<A>(symbol));
            }
            else
            {
                --value(state);
            }
        }

        Integer n;
    };

    /*!
        \~english
            \brief
                Creates a fixed size prefix dropping transducer

            \param n
                The amount of symbols to drop.

            \pre
                `n >= 0`

            \returns
                An instance of `drop_t`.

        \~russian
            \brief
                Создать преобразователь, отбрасывающий префикс заданной длины

            \param n
                Количество символов, которое требуется отбросить.

            \pre
                `n >= 0`

            \returns
                Экземпляр `drop_t`.

        \~  \see drop_t
            \see take
            \see take_if
            \see drop_if
            \see take_while
            \see drop_while
     */
    template <typename Integer>
    constexpr auto drop (Integer n)
    {
        static_assert(std::is_integral_v<Integer>);
        assert(n >= Integer{0});
        return drop_t<Integer>{n};
    }

    template <typename Integer, typename A>
    constexpr auto initialize (const drop_t<Integer> & drop, type_t<A>)
    {
        return simple_state_t<Integer, A>{drop.n};
    }
}
