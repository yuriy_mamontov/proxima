#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <cassert>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                A transducer that takes the prefix of the sequence, all symbols of which prefix
                satisfy a predicate

            \tparam UnaryPredicate
                A predicate, which will be applied to symbols from the input tape.

        \~russian
            \brief
                Преобразователь, отбирающий префикс последовательности, все символы которого
                удовлетворяют предикату

            \tparam UnaryPredicate
                Одноместный предикат, на соответствие которому будут проверяться символы со входной
                ленты.

        \~  \see take_while
            \see take_t
            \see take_if_t
            \see drop_t
            \see drop_if_t
            \see drop_while_t
     */
    template <typename UnaryPredicate>
    struct take_while_t
    {
        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape and checks if it satifies the predicate.
                    If the predicate returns true, then the symbol is written to the output tape.
                    Otherwise nothing is written to the output tape and the state becomes final.

                \pre
                    The state is not final, i.e.:

                        is_final(*this, state) == false

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. In case of success the result will be written to this tape.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты и проверяет его на соответствие предикату.
                    Если предикат возвращает истину на символе, то символ записывается на выходную
                    ленту. В противном случае на выходную ленту не пишется ничего, а состояние
                    становится финальным.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую будет записан результат в случае успеха.
                \param state
                    Текущее состояние преобразователя.

                \pre
                    Состояние преобразователя не финально, то есть:

                        is_final(*this, state) == false
         */
        template <typename A, typename T, typename S>
        constexpr void operator () (A && symbol, T && tape, S & state) const
        {
            assert(value(state) == running);
            if (p(symbol))
            {
                std::forward<T>(tape)(std::forward<A>(symbol));
            }
            else
            {
                value(state) = stopped;
            }
        }

        static constexpr auto stopped = false;
        static constexpr auto running = true;

        UnaryPredicate p;
    };

    /*!
        \~english
            \brief
                Creates the conditional prefix taking transducer

            \param p
                A predicate that will be applied to the symbols from the input tape.

            \returns
                An instance of `take_while_t`.

        \~russian
            \brief
                Создание преобразователя, отбирающего префикс последовательности по условию

            \param p
                Предикат, на соответствие которому преобразователь будет проверять символы со
                входной ленты.

            \returns
                Экземпляр `take_while_t`.

        \~  \see take_while_t
            \see drop_while
            \see take_if
            \see drop_if
            \see take
            \see drop
     */
    template <typename UnaryPredicate>
    constexpr auto take_while (UnaryPredicate && p)
        -> take_while_t<std::decay_t<UnaryPredicate>>
    {
        return {std::forward<UnaryPredicate>(p)};
    }

    template <typename UnaryPredicate, typename A>
    constexpr auto initialize (const take_while_t<UnaryPredicate> &, type_t<A>)
    {
        return simple_state_t<bool, A>{take_while_t<UnaryPredicate>::running};
    }

    template <typename UnaryPredicate, typename V, typename A>
    constexpr bool is_final (const take_while_t<UnaryPredicate> &, const simple_state_t<V, A> & s)
    {
        return value(s) == take_while_t<UnaryPredicate>::stopped;
    }
}
