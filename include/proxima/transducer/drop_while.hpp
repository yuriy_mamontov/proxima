#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <cassert>
#include <functional>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                A transducer that drops the longest prefix of the sequence, all symbols of which
                prefix satisfy a predicate

            \tparam UnaryPredicate
                The type of a predicate, which will be applied to symbols from the input tape.

        \~russian
            \brief
                Преобразователь, игнорирующий наибольший префикс последовательности, такой, что все
                символы этого префикса удовлетворяют предикату

            \tparam UnaryPredicate
                Одноместный предикат, на соответствие которому будут проверяться символы со входной
                ленты.

        \~  \see drop_while
            \see take_t
            \see take_if_t
            \see drop_t
            \see drop_if_t
            \see take_while_t
     */
    template <typename UnaryPredicate>
    struct drop_while_t
    {
        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape and checks if it satifies the predicate.
                    If the predicate returns true, then the symbol is ignored, i.e. nothing is
                    written to the output tape. Otherwise the symbol is written to the output tape
                    and the state changes so the predicate is checked no more and all the symbols
                    from the input tape will be written to the output tape.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The result will be written to this tape, if necessary.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты и проверяет его на соответствие предикату.
                    Если предикат возвращает истину на символе, то символ игнорируется, то есть на
                    выходную ленту ничего не записывается. В противном случае символ записывается на
                    выходную ленту, а состояние преходит в позицию, когда проверка предиката не
                    производится, а берутся все символы без разбора.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую при необходимости будет записан результат.
                \param state
                    Текущее состояние преобразователя.
         */
        template <typename A, typename T, typename S>
        constexpr void operator () (A && symbol, T && tape, S & state) const
        {
            if (value(state) == take_everything)
            {
                std::forward<T>(tape)(std::forward<A>(symbol));
            }
            else if (not p(std::as_const(symbol)))
            {
                assert(value(state) == check);
                value(state) = take_everything;
                std::forward<T>(tape)(std::forward<A>(symbol));
            }
        }

        static constexpr auto take_everything = false;
        static constexpr auto check = true;

        UnaryPredicate p;
    };

    /*!
        \~english
            \brief
                Creates the conditional prefix dropping transducer

            \param p
                A predicate that will be applied to the symbols from the input tape.

            \returns
                An instance of `drop_while_t`.

        \~russian
            \brief
                Создание преобразователя, игнорирующего префикс последовательности по условию

            \param p
                Предикат, на соответствие которому преобразователь будет проверять символы со
                входной ленты.

            \returns
                Экземпляр `drop_while_t`.

        \~  \see drop_while_t
            \see take_while
            \see take_if
            \see drop_if
            \see take
            \see drop
     */
    template <typename UnaryPredicate>
    constexpr auto drop_while (UnaryPredicate && p)
        -> drop_while_t<std::decay_t<UnaryPredicate>>
    {
        return {std::forward<UnaryPredicate>(p)};
    }

    template <typename UnaryPredicate, typename A>
    constexpr auto initialize (const drop_while_t<UnaryPredicate> &, type_t<A>)
    {
        return simple_state_t<bool, A>{drop_while_t<UnaryPredicate>::check};
    }
}
