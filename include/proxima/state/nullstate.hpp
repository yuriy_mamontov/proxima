#pragma once

namespace proxima
{
    struct nullstate_t {};
    constexpr auto nullstate = nullstate_t{};
} // namespace proxima
