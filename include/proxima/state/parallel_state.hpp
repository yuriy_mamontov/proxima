#pragma once

#include <proxima/thread/thread_pool.hpp>
#include <proxima/type_traits/state_of.hpp>
#include <proxima/type_traits/state_value.hpp>

#include <memory>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                The state of parallel reduce kernel

            \details
                Contains the state of the reduce kernel to be executed asynchronously.
                Also it contains the thread pool, which will provide asynchronous execution
                of the wrapped reduce kernel.

            \tparam K
                The reduce kernel which will be executed in a separate thread.
            \tparam A
                Type of the symbol from which the state of the wrapped reduce kernel `K` was
                initialized.

        \~russian
            \brief
                Состояние параллельного ядра свёртки

            \details
                Содержит состояние ядра свёртки, которое будет вызываться асинхронно.
                Помимо этого содержит сам пул потоков, который и будет обеспечивать асинхронную
                работу обёрнутого ядра свёртки.

            \tparam K
                Ядро свёртки, которое будет выполняться в отдельном потоке.
            \tparam A
                Тип символа, из которого было проинициализировано состояние ядра свёртки `K`.

        \~  \see initialize
            \see parallel_reduce_kernel_t
            \see parallel_state_synchronization_t
     */
    template <typename K, typename A>
    struct parallel_state_t
    {
        using wrapped_state_type = state_of_t<K, A>;

        using value_type = state_value_t<wrapped_state_type>;
        using symbol_type = A;

        std::unique_ptr<wrapped_state_type> wrapped_state;
        std::unique_ptr<thread_pool> pool;
    };

    template <typename K, typename A>
    constexpr auto & value (parallel_state_t<K, A> & state)
    {
        return value(*state.wrapped_state);
    }

    template <typename K, typename A>
    constexpr const auto & value (const parallel_state_t<K, A> & state)
    {
        return value(*state.wrapped_state);
    }

    template <typename K, typename A>
    constexpr auto value (parallel_state_t<K, A> && state)
    {
        return value(*std::move(state).wrapped_state);
    }
} // namespace proxima
