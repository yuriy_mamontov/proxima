#pragma once

#include <proxima/kernel/composite_reduce_kernel.hpp>

#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Composition of transducers and reduce kernel

            \details
                Receives several transducers and one, and only one reduce kernel standing in the
                last position.

                    xs = t_1, ..., t_n, k

                where each `t_i` is a transducer, `k` is a reduce kernel.

                Composition is right-associative.

                    compose(t_1, ..., t_n, k) = compose(t_1, compose(..., compose(t_n, k)))

            \param xs
                Transducers and a reduce kernel.

            \returns
                A composite reduce kernel.

        \~russian
            \brief
                Композиция преобразователей и ядра свёртки

            \details
                Принимает несколько преобразователей и одно, и только одно ядро свёртки, стоящее на
                последнем месте.

                    xs = t_1, ..., t_n, k

                где каждое `t_i` — преобразователь, `k` — ядро свёртки.

                Композиция правоассоциативна.

                    compose(t_1, ..., t_n, k) = compose(t_1, compose(..., compose(t_n, k)))

            \param xs
                Преобразователи и ядро свёртки.

            \returns
                Составное ядро свёртки.

        \~  \see composite_reduce_kernel_t
     */
    template <typename... Ts>
    constexpr auto compose (Ts &&... xs);

    template <typename Head, typename... Tail>
    constexpr auto compose (Head && head, Tail &&... tail)
    {
        using transducer_type = std::decay_t<Head>;
        using kernel_type = decltype(compose(std::forward<Tail>(tail)...));
        return
            composite_reduce_kernel_t<transducer_type, kernel_type>
            {
                std::forward<Head>(head),
                compose(std::forward<Tail>(tail)...)
            };
    }

    template <typename K>
    constexpr auto compose (K && kernel)
    {
        return std::forward<K>(kernel);
    }
} // namespace proxima
