#include <proxima/compose.hpp>
#include <proxima/kernel/fold.hpp>
#include <proxima/transducer/group_by.hpp>
#include <proxima/transducer/take.hpp>
#include <proxima/transducer/transform.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/is_emitting.hpp>
#include <proxima/type_traits/is_finalizable.hpp>
#include <proxima/type_traits/is_reducible.hpp>

#include <catch2/catch.hpp>

TEST_CASE("Композиция преобразователей и ядра свёртки является ядром свёртки", "[compose]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::fold(0.0, [](auto x, auto y) {return x + y;})
        );
    REQUIRE(proxima::is_reducible_v<decltype(k), int>);
}

TEST_CASE("Композиция нефинализируемых преобразователей нефинализируема", "[compose]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::fold(0.0, [] (auto x, auto y) {return x + y;})
        );
    REQUIRE_FALSE(proxima::is_finalizable_v<decltype(k), int>);
}

TEST_CASE("Композиция непорождающих преобразователей не является порождающей", "[compose]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::fold(0.0, [] (auto x, auto y) {return x + y;})
        );
    REQUIRE_FALSE(proxima::is_emitting_v<decltype(k), int>);
}

TEST_CASE("Композиция финализируемых преобразователей финализируема", "[compose]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::take(5),
            proxima::fold(0.0, [] (auto x, auto y) {return x + y;})
        );
    REQUIRE(proxima::is_finalizable_v<decltype(k), int>);
}

TEST_CASE("Композиция преобразователей, среди которых есть хотя бы один в финальном состоянии, "
    "является финальной", "[compose]")
{
    const auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::take(1),
            proxima::fold(0.0, [] (auto x, auto y) {return x + y;})
        );
    auto s = initialize(k, proxima::type<int>);

    REQUIRE(not proxima::is_final(k, s));
    k(3.14, s);
    REQUIRE(proxima::is_final(k, s));
}

TEST_CASE("Композиция порождающих преобразователей является порождающией", "[compose]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::group_by(std::equal_to<>{}),
            proxima::fold(0ul, [] (auto x, auto y) {return x + y.size();})
        );
    REQUIRE(proxima::is_emitting_v<decltype(k), int>);
}

TEST_CASE("Композиция преобразователей, среди которых есть и финализируемые, и порождающие, "
    "является и финализируемой, и порождающей", "[compose]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::take(5),
            proxima::group_by(std::equal_to<>{}),
            proxima::fold(0ul, [] (auto x, auto y) {return x + y.size();})
        );
    REQUIRE(proxima::is_emitting_v<decltype(k), int>);
    REQUIRE(proxima::is_finalizable_v<decltype(k), int>);
}
