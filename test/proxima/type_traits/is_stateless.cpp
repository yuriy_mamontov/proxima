#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/is_stateless.hpp>

#include <catch2/catch.hpp>

namespace // anonymous
{
    struct stateful_pseudokernel_t {};

    template <typename V>
    constexpr auto initialize (const stateful_pseudokernel_t &, proxima::type_t<V>)
    {
        return proxima::simple_state_t<V>{V{}};
    }

    struct stateless_pseudokernel_t {};
} // namespace anonymous

TEST_CASE("Метафункция \"is_stateless\"", "[stateful]")
{
    SECTION("возвращает ложь на ядре с состоянием")
    {
        CHECK_FALSE(proxima::is_stateless_v<stateful_pseudokernel_t, int>);
    }
    SECTION("возвращает истину на ядре без состояния")
    {
        CHECK(proxima::is_stateless_v<stateless_pseudokernel_t, int>);
    }
    SECTION("возвращает истину на типе, не являющимся ядром")
    {
        CHECK(proxima::is_stateless_v<double, int>);
    }
}
