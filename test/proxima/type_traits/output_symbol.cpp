#include <proxima/type_traits/output_symbol.hpp>

#include <catch2/catch.hpp>

#include <type_traits>
#include <utility>

namespace // anonymous
{
    struct simple_pseudokernel_t {};

    struct complex_pseudokernel_t
    {
        template <typename A>
        using output_symbol_t = std::pair<A, int>;
    };
} // namespace anonymous

namespace proxima
{
    struct simple_pseudokernel_t {};

    template <typename Symbol>
    struct output_symbol<simple_pseudokernel_t, Symbol>
    {
        using type = Symbol;
    };
}

TEST_CASE("Метафункция \"output_symbol\"", "[output_symbol]")
{
    SECTION("возвращает входной тип символа, если у него не определена "
        "собственная метафункция \"output_symbol_t\"")
    {
        CHECK(std::is_same_v<proxima::output_symbol<simple_pseudokernel_t, int>::type, int>);
        CHECK(std::is_same_v<proxima::output_symbol_t<simple_pseudokernel_t, int>, int>);
    }
    SECTION("возвращает результат метафункции ядра, если она есть")
    {
        CHECK(std::is_same_v
        <
            proxima::output_symbol_t<complex_pseudokernel_t, double>,
            complex_pseudokernel_t::output_symbol_t<double>
        >);
        CHECK(std::is_same_v
        <
            proxima::output_symbol_t<complex_pseudokernel_t, double>,
            std::pair<double, int>
        >);
    }
}

TEST_CASE("Метафункция \"output_symbol\" игнорирует модификатор \"const\"","[output_symbol]")
{
    CHECK(std::is_same_v<proxima::output_symbol_t<const simple_pseudokernel_t, int>, int>);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const complex_pseudokernel_t, double>,
        std::pair<double, int>
    >);
}

TEST_CASE("Метафункция \"output_symbol\" игнорирует модификатор \"volatile\"","[output_symbol]")
{
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<volatile simple_pseudokernel_t, int>,
        int
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<volatile complex_pseudokernel_t, int>,
        std::pair<int, int>
    >);
}

TEST_CASE("Метафункция \"output_symbol\" игнорирует ссылку","[output_symbol]")
{
    CHECK(std::is_same_v<proxima::output_symbol_t<simple_pseudokernel_t &, int>, int>);
    CHECK(std::is_same_v<proxima::output_symbol_t<simple_pseudokernel_t &&, int>, int>);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<complex_pseudokernel_t &, int>,
        std::pair<int, int>
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<complex_pseudokernel_t &&, char>,
        std::pair<char, int>
    >);
}

TEST_CASE("Метафункция \"output_symbol\" игнорирует любые сочетания"
    " модификаторов и ссылок","[output_symbol]")
{
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile simple_pseudokernel_t &, int>,
        int
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile simple_pseudokernel_t &&, int>,
        int
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile complex_pseudokernel_t &, int>,
        std::pair<int, int>
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile complex_pseudokernel_t &&, int>,
        std::pair<int, int>
    >);
}

TEST_CASE("Специализация метафункции \"output_symbol\" игнорирует"
    " модификатор \"const\"","[output_symbol]")
{
    CHECK(std::is_same_v<proxima::output_symbol_t<const proxima::simple_pseudokernel_t, int>, int>);
}

TEST_CASE("Специализация метафункции \"output_symbol\" игнорирует"
    " модификатор \"volatile\"","[output_symbol]")
{
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<volatile proxima::simple_pseudokernel_t, int>,
        int
    >);
}

TEST_CASE("Специализация метафункции \"output_symbol\" игнорирует ссылку","[output_symbol]")
{
    CHECK(std::is_same_v<proxima::output_symbol_t<proxima::simple_pseudokernel_t &, int>, int>);
    CHECK(std::is_same_v<proxima::output_symbol_t<proxima::simple_pseudokernel_t &&, int>, int>);
}

TEST_CASE("Специализация метафункции \"output_symbol\" игнорирует любые сочетания"
    " модификаторов и ссылок","[output_symbol]")
{
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile proxima::simple_pseudokernel_t &, int>,
        int
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile proxima::simple_pseudokernel_t &&, int>,
        int
    >);
}
