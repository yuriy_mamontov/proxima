#include <proxima/transducer/exclusive_scan.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <functional>
#include <string>
#include <type_traits>

TEST_CASE("Тип выходного символа преобразователя \"exclusive_scan\" совпадает с типом начального "
    "значения", "[exclusive_scan][transducer]")
{
    using input_symbol_type = int;
    using initial_value_type = double;
    using output_symbol_type =
        proxima::output_symbol_t
        <
            decltype(proxima::exclusive_scan(initial_value_type{0}, std::plus<>{})),
            input_symbol_type
        >;

    CHECK(std::is_same_v<output_symbol_type, initial_value_type>);
}

TEST_CASE("Преобразователь \"exclusive_scan\" на первом шаге возвращает начальное значение",
    "[exclusive_scan][transducer]")
{
    const auto t = proxima::exclusive_scan(3, std::multiplies<>{});
    auto state = proxima::initialize(t, proxima::type<int>);

    auto [_, result] = proxima::utility::invoke(t, 17, state);
    static_cast<void>(_);

    CHECK(result == 3);
}

TEST_CASE("Преобразователь \"exclusive_scan\" возвращает исключительную префиксную сумму "
    "пройденных символов относительно начального значения", "[exclusive_scan][transducer]")
{
    const auto t = proxima::exclusive_scan(std::string(u8"ноль"), std::plus<>{});
    auto state0 = proxima::initialize(t, proxima::type<std::string>);

    const auto [state1, symbol1] = proxima::utility::invoke(t, std::string(u8"раз"), state0);
    CHECK(symbol1 == u8"ноль");
    const auto [state2, symbol2] = proxima::utility::invoke(t, std::string(u8"два"), state1);
    CHECK(symbol2 == u8"нольраз");
    const auto [state3, symbol3] = proxima::utility::invoke(t, std::string(u8"три"), state2);
    static_cast<void>(state3);
    CHECK(symbol3 == u8"нольраздва");
}

TEST_CASE("Преобразователь \"exclusive_scan\" умеет работать на этапе компиляции",
    "[exclusive_scan][transducer]")
{
    constexpr auto t = proxima::exclusive_scan(7, [] (auto x, auto y) {return (x + y) % 19;});
    constexpr auto s = proxima::initialize(t, proxima::type<int>);

    constexpr auto r = proxima::utility::invoke(t, 15, s);
    constexpr auto x = r.second;

    static_assert(x == 7);
    CHECK(x == 7);
}
