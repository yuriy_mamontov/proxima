#include <proxima/transducer/transform.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <functional>
#include <type_traits>

TEST_CASE("Преобразователь \"transform\" записывает на ленту результат применения запомненной"
    "функции к входному символу", "[transform]")
{
    SECTION("на этапе исполнения")
    {
        const auto pow2 = [] (auto value) {return value * value;};
        const auto value = 2;
        const auto expected_result = pow2(value);

        const auto tr = proxima::transform(pow2);
        const auto result = proxima::utility::invoke(tr, value);

        REQUIRE(result == expected_result);
    }

    SECTION("на этапе компиляции")
    {
        constexpr auto value = 2;
        constexpr auto pow2 = [] (auto value) constexpr {return value * value;};
        constexpr auto expected_result = pow2(value);
        constexpr auto tr = proxima::transform(pow2);
        constexpr auto result = proxima::utility::invoke(tr, value);
        static_assert(result == expected_result);
        REQUIRE(result == expected_result);
    }
}

TEST_CASE("Перегрузка \"output_symbol\"", "[transform] [output_symbol]")
{
    SECTION("возвращает тип, в точности равный типу,"
        "возвращаемому функцией, запомненной в преобразователе")
    {
        using input_symbol_type = int;

        constexpr auto pow2_fn = [] (auto value) constexpr {return value * value;};
        using fn_result_type = std::invoke_result_t<decltype(pow2_fn), input_symbol_type>;

        constexpr auto tr = proxima::transform(pow2_fn);
        using output_symbol = proxima::output_symbol_t<decltype(tr), input_symbol_type>;

        REQUIRE(std::is_same_v<fn_result_type, output_symbol>);
    }
}
