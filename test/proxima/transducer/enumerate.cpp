#include <proxima/transducer/enumerate.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <cstddef>
#include <string>
#include <type_traits>
#include <utility>

TEST_CASE("Тип выходного символа преобразователя \"enumerate\" является парой из типа входного "
    "символа и его номера относительно входной ленты (по-умолчанию — std::size_t)",
    "[enumerate][transducer]")
{
    using input_symbol_type = double;

    using output_symbol_type =
        proxima::output_symbol_t<decltype(proxima::enumerate), input_symbol_type>;

    REQUIRE(std::is_same_v<output_symbol_type, std::pair<input_symbol_type, std::size_t>>);
}

TEST_CASE("Тип счётчика преобразователя \"enumerate\" может определяться начальным значением",
    "[enumerate][transducer]")
{
    using input_symbol_type = double;
    auto enumerate = proxima::enumerate(char{5});
    using output_symbol_type =
        proxima::output_symbol_t<decltype(enumerate), input_symbol_type>;

    REQUIRE(std::is_same_v<output_symbol_type, std::pair<input_symbol_type, char>>);
}

TEST_CASE("Преобразователь \"enumerate\" на первом шаге записывает на выходную ленту пару из того "
    "символа, который принял на вход, и начального значения счётчика", "[enumerate][transducer]")
{
    const auto initial_value = 13;
    const auto t = proxima::enumerate(initial_value);
    auto state = proxima::initialize(t, proxima::type<std::string>);

    auto [_, result] = proxima::utility::invoke(t, std::string("qwe"), state);
    static_cast<void>(_);

    REQUIRE(result.first == std::string("qwe"));
    REQUIRE(result.second == initial_value);
}

TEST_CASE("Начальное значение счётчика \"enumerate\" по-умолчанию — 0", "[enumerate][transducer]")
{
    const auto t = proxima::enumerate;
    auto state = proxima::initialize(t, proxima::type<std::string>);

    auto [_, result] = proxima::utility::invoke(t, std::string("qwe"), state);
    static_cast<void>(_);

    REQUIRE(result == std::pair(std::string("qwe"), std::size_t{0}));
}

TEST_CASE("Преобразователь \"enumerate\" записывает на выходную ленту пару из текущего символа, "
    "считанного со входной ленты, и числа, равного начальному значению плюс количество символов, "
    "считанных со входной ленты без учёта текущего символа", "[enumerate][transducer]")
{
    const auto t = proxima::enumerate(1);
    auto state0 = proxima::initialize(t, proxima::type<std::string>);

    const auto [state1, symbol1] = proxima::utility::invoke(t, std::string(u8"раз"), state0);
    REQUIRE(symbol1 == std::pair(std::string(u8"раз"), 1));
    const auto [state2, symbol2] = proxima::utility::invoke(t, std::string(u8"два"), state1);
    REQUIRE(symbol2 == std::pair(std::string(u8"два"), 2));
    const auto [state3, symbol3] = proxima::utility::invoke(t, std::string(u8"три"), state2);
    REQUIRE(symbol3 == std::pair(std::string(u8"три"), 3));
    static_cast<void>(state3);
}

// Раскомментировать при переходе на C++20.
// TEST_CASE("Преобразователь \"enumerate\" может быть вычислен на этапе компиляции",
//     "[enumerate][transducer]")
// {
//     constexpr auto enumerate = proxima::enumerate(-5);
//     constexpr auto s0 = proxima::initialize(enumerate, proxima::type<char>);

//     constexpr auto r1 = proxima::utility::invoke(enumerate, 'x', s0);
//     static_assert(r1.second.second == -5);
//     REQUIRE(r1.second.second == -5);

//     constexpr auto r2 = proxima::utility::invoke(enumerate, 'y', r1.first);
//     static_assert(r2.second.second == -4);
//     REQUIRE(r2.second.second == -4);
// }
