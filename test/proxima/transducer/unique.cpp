#include <proxima/transducer/unique.hpp>
#include <proxima/type_traits/output_symbol.hpp>

#include <catch2/catch.hpp>

#include <string>
#include <type_traits>
#include <vector>

TEST_CASE("Тип выходного символа преобразователя \"unique\" совпадает с типом входного символа",
    "[unique][transducer]")
{
    using input_symbol_type = double;
    using output_symbol_type =
        proxima::output_symbol_t<decltype(proxima::unique), input_symbol_type>;

    REQUIRE(std::is_same_v<input_symbol_type, output_symbol_type>);
}

TEST_CASE("Преобразователь \"unique\" оставляет только один символ из группы одинаковых символов",
    "[unique][transducer]")
{
    const auto t = proxima::unique;
    auto state = proxima::initialize(t, proxima::type<char>);

    std::vector<char> result;
    const auto tape = [& result] (auto c) {result.push_back(c);};
    for (auto symbol: {'a', 'a', 'a', 'b', 'b', 'c'})
    {
        t(symbol, tape, state);
    }

    const auto expected_result = std::vector{'a', 'b', 'c'};
    REQUIRE(result == expected_result);
}

TEST_CASE("Преобразователь \"unique\" оставляет строго первый символ из группы одинаковых символов",
    "[unique][transducer]")
{
    const auto t = proxima::unique([] (auto x, auto y) {return x.size() == y.size();});
    auto state = proxima::initialize(t, proxima::type<std::string>);

    const auto symbols = std::vector<std::string>{u8"раз", u8"два", u8"три"};

    std::string result;
    const auto tape = [& result] (auto s) {result = s;};
    for (auto symbol: symbols)
    {
        t(symbol, tape, state);
    }

    REQUIRE(result == u8"раз");
}

TEST_CASE("Преобразователь \"unique\" пропускает одинаковые символы, если между ними есть хотя бы "
    "один иной символ", "[unique][transducer]")
{
    const auto t = proxima::unique;
    auto state = proxima::initialize(t, proxima::type<char>);

    std::vector<char> result;
    const auto tape = [& result] (auto c) {result.push_back(c);};
    for (auto symbol: {'a', 'a', 'a', 'b', 'a', 'a'})
    {
        t(symbol, tape, state);
    }

    const auto expected_result = std::vector{'a', 'b', 'a'};
    REQUIRE(result == expected_result);
}

TEST_CASE("Преобразователь \"unique\" допускает пользовательский предикат", "[unique][transducer]")
{
    const auto is_divided = [] (auto previous, auto current) {return previous % current == 0;};
    const auto t = proxima::unique(is_divided);
    auto state = proxima::initialize(t, proxima::type<int>);

    std::vector<int> result;
    const auto tape = [& result] (auto c) {result.push_back(c);};
    for (auto symbol: {15, 5, 3, 4, 2, 2, 3})
    {
        t(symbol, tape, state);
    }

    const auto expected_result = std::vector{15, 4, 3};
    REQUIRE(result == expected_result);
}
