#include <proxima/transducer/take_if.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

TEST_CASE("Преобразователь \"take_if\" записывает на выходную ленту элементы, удовлетворяющие "
    "предикату", "[take_if][transducer]")
{
    const auto t = proxima::take_if([] (auto x) {return x < 5;});

    int result;
    auto tape = [& result] (auto x) {result = x;};

    t(3, tape);
    REQUIRE(result == 3);
}

TEST_CASE("Преобразователь \"take_if\" не записывает на выходную ленту элементы, не "
    "удовлетворяющие предикату", "[take_if][transducer]")
{
    const auto t = proxima::take_if([] (auto x) {return x < 5;});

    auto call_count = 0;
    auto tape = [& call_count] (auto) {++call_count;};

    t(10, tape);
    REQUIRE(call_count == 0);
}

TEST_CASE("Тип выходного символа преобразователя \"take_if\" совпадает с типом входного символа",
    "[take_if][transducer]")
{
    auto t = proxima::take_if([] (auto x) {return x == 17;});

    using input_symbol_type = short;
    using output_symbol_type = proxima::output_symbol_t<decltype(t), input_symbol_type>;
    REQUIRE(std::is_same_v<output_symbol_type, input_symbol_type>);
}

TEST_CASE("Преобразователь \"take_if\" может быть выполнен на этапе компиляции",
    "[take_if][transducer]")
{
    constexpr auto t = proxima::take_if([] (auto x) {return x == 'x';});

    constexpr auto x = proxima::utility::invoke(t, 'x');
    static_assert(x == 'x');
    REQUIRE(x == 'x');
}
