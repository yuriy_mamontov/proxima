#include <proxima/transducer/take_while.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <type_traits>

TEST_CASE("Новосозданное состояние преобразователя \"take_while\" не является финальным",
    "[take_while][transducer]")
{
    const auto t = proxima::take_while([] (auto x) {return x < 5;});
    auto s = proxima::initialize(t, proxima::type<int>);

    REQUIRE(not proxima::is_final(t, s));
}

TEST_CASE("Состояние преобразователя \"take_while\" остаётся нефинальным, если со входной ленты "
    "приходят символы, удовлетворяющие предикату", "[take_while][transducer]")
{
    const auto t = proxima::take_while([] (auto x) {return x < 5;});
    auto s = proxima::initialize(t, proxima::type<int>);

    const auto tape = [] (auto) {};

    t(1, tape, s);
    REQUIRE(not proxima::is_final(t, s));

    t(2, tape, s);
    REQUIRE(not proxima::is_final(t, s));

    t(3, tape, s);
    REQUIRE(not proxima::is_final(t, s));
}

TEST_CASE("Состояние преобразователя \"take_while\" становится финальным, как только со входной "
    "ленты приходит символ, не удовлетворяющий предикату", "[take_while][transducer]")
{
    const auto t = proxima::take_while([] (auto x) {return x < 5;});
    auto s = proxima::initialize(t, proxima::type<int>);

    const auto tape = [] (auto) {};

    t(10, tape, s);

    REQUIRE(proxima::is_final(t, s));
}

TEST_CASE("Преобразователь \"take_while\" записывает на выходную ленту символы, удовлетворяющие "
    "предикату", "[take_while][transducer]")
{
    const auto t = proxima::take_while([] (auto x) {return x.size() > 0;});
    const auto s = proxima::initialize(t, proxima::type<std::string>);

    const auto [_, x] = proxima::utility::invoke(t, std::string("qwe"), s);
    static_cast<void>(_);

    REQUIRE(x == "qwe");
}

TEST_CASE("Преобразователь \"take_while\" не записывает на выходную ленту символ, не "
    "удовлетворяющий предикату", "[take_while][transducer]")
{
    const auto t = proxima::take_while([] (auto x) {return x.size() > 0;});
    auto s = proxima::initialize(t, proxima::type<std::string>);

    auto call_count = 0;
    auto tape = [& call_count] (auto) {++call_count;};

    t(std::string{}, tape, s);
    REQUIRE(call_count == 0);
}

TEST_CASE("Тип выходного символа преобразователя \"take_while\" совпадает с типом входного символа",
    "[take_while][transducer]")
{
    auto t = proxima::take_while([] (auto x) {return x == 17;});

    using input_symbol_type = short;
    using output_symbol_type = proxima::output_symbol_t<decltype(t), input_symbol_type>;
    REQUIRE(std::is_same_v<output_symbol_type, input_symbol_type>);
}

TEST_CASE("Преобразователь \"take_while\" может быть выполнен на этапе компиляции",
    "[take_while][transducer]")
{
    constexpr auto t = proxima::take_while([] (auto x) {return x == 'x';});
    constexpr auto s = proxima::initialize(t, proxima::type<char>);

    constexpr auto r = proxima::utility::invoke(t, 'x', s);
    constexpr auto x = r.second;
    static_assert(x == 'x');
    REQUIRE(x == 'x');
}
